.. filtools documentation master file, created by
   sphinx-quickstart on Sat Apr 20 17:31:22 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to filtools's documentation!
====================================

.. toctree::
   :maxdepth: 4
   :caption: User Guide

   introduction
   installation
   programs

.. toctree::
   :maxdepth: 4
   :caption: Class desriptions

   filtools
   filtools.inject_pulsar




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
