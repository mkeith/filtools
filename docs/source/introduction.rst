
Introduction
============

.. toctree::
   :maxdepth: 4

Filtools is a collection of tools for working with pulsar filterbank data. The intent is to provide a collection of useful library routines and programs 
for solving tricky problems when working with filterbank data. Initially this will focus on sigproc format filtebank files, but may extend to presto and psrfits
format files in future.

As of writing, this is only just starting and contains two useful components: the :class:`filtools.FilterbankIO` class for reading and writing filtebank data, and a series of routines and software for injecting simulated pulsar signals into filterbank files.

