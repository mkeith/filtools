Installation
============


filtools can be installed using pip::

   pip install git+https://bitbucket.org/mkeith/filtools


Or directly by downloading the git repository::
   
   git clone https://bitbucket.org/mkeith/filtools
   cd filtools
   python setup.py install

The git repository can be viewed at https://bitbucket.org/mkeith/filtools   

Requirements
------------

 * Python 2.7 or 3.6+
 * numpy
 * yaml

Some routines can also make use of tempo2 predictors by installing the t2pred python library from tempo2.
