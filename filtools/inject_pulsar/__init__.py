r"""
This package contains classes designed to aid injection of pulsar
and other similar signals into filterbank data
"""


from .phase_models import *
from .pulse_models import *
from .ism_models import *
from .inject_rfi import *


