# filtools
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5141586.svg)](https://doi.org/10.5281/zenodo.5141586)


Filtools is a collection of tools for working with filtebank data. Also incldues programs for simulating pulsar signals.


More complete documentation can be found at https://filtools.readthedocs.io/

